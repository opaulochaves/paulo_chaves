# Frontend-Test Styling - JS

[Go to the live test](https://frontend-test-2017.firebaseapp.com)

## Technologies

- CSS & SASS
- ES6 compiled with Babel
- Webpack
- Gulp
- Pug
- Firebase Hosting
- Git

### References
- [Can I Use](http://caniuse.com)
- [You Don't Need jQuery](https://github.com/oneuijs/You-Dont-Need-jQuery)
- [MDN - Web technology for developers](https://developer.mozilla.org/en-US/docs/Web)
- [A Complete Guide to Flexbox](css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Gulp Webpack Starter](https://github.com/wwwebman/gulp-webpack-starter)
