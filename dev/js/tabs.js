import $, { parent } from './modules/helpers'

const active = 'tabs__item--active'

function tabs() {
  function openTab(target, tabsClass) {
    const tabsToHide = $(tabsClass, true)
    if (tabsToHide) {
      for (const tabArea of tabsToHide) {
        tabArea.style.display = 'none'
      }
    }

    const tabArea = $(target.dataset.tab)
    tabArea.style.display = 'block'
    setActiveTab(target)
  }

  function setActiveTab(clickedTab) {
    const tabsLinks = $('.tabs__item', true)
    for (const item of tabsLinks) {
      item.classList.remove(active)
    }
    parent(clickedTab).classList.add(active)
  }

  const showActiveTab = () => {
    const activeTab = $(`.${active} > a`)
    if (activeTab) {
      const tabArea = activeTab.dataset.tab
      $(tabArea).style.display = 'block'
    }
  }

  return {
    openTab,
    showActiveTab
  }
}

export default tabs()
