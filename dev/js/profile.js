import $, { setFieldValue } from './modules/helpers'

function profile() {
  const saveForm = () => {
    const fullname = `${$('#firstname').value} ${$('#lastname').value}`
    setFieldValue('name', fullname)
    setFieldValue('website', $('#website').value)
    setFieldValue('phone', $('#phone').value)
    setFieldValue('address', $('#address').value)
    cancelEdit()
  }

  const cancelEdit = (oldValues) => {
    const hidden = 'profile--hidden'
    $('.profile__edit__btn').classList.remove(hidden)
    $('.profile__btns').classList.add(hidden)

    const labels = $('.profile__label', true)
    for (const elm of labels) {
      elm.classList.remove(hidden)
    }

    const fields = $('.profile__field', true)
    for (const elm of fields) {
      elm.classList.add(hidden)
    }

    if (oldValues) {
      $('#firstname').value = oldValues.firstname
      $('#lastname').value = oldValues.lastname
      $('#website').value = oldValues.website
      $('#phone').value = oldValues.phone
      $('#address').value = oldValues.address
    }
  }

  const enableEditFields = () => {
    const hidden = 'profile--hidden'
    $('.profile__edit__btn').classList.add(hidden)
    $('.profile__btns').classList.remove(hidden)

    const labels = $('.profile__label', true)
    for (const elm of labels) {
      elm.classList.add(hidden)
    }

    const fields = $('.profile__field', true)
    for (const elm of fields) {
      elm.classList.remove(hidden)
    }
  }

  return {
    saveForm,
    cancelEdit,
    enableEditFields,
  }
}

export default profile()
