import $, {
  onClick,
  onClickOff,
  attr,
  setFieldValue } from './modules/helpers'

function buildPopup({
  position,
  parent,
  parentPosition,
  fieldName
}) {
  const existingPopup = $('.profile__popup')
  if (existingPopup) {
    parent.removeChild(existingPopup)
  }

  const {value, label} = getFieldLabelAndValue(fieldName)

  const popup = document.createElement('div')
  popup.innerHTML = `
    <div class="arrow--left"></div>
    <div class="input-group">
      <input type="text" id="popup__field" data-field="${fieldName}" value="${value}" required/>
      <label>${label}</label>
    </div>
    <div class="row popup__btns">
      <div>
        <button class="btn btn--blue popup__btns__save">Save</button>
      </div>
      <div>
        <button class="btn btn--white popup__btns__cancel">Cancel</button>
      </div>
    </div>
  `

  const center = (position.top - parentPosition.top) - (position.height / 2)
  const left = position.left - parentPosition.left

  popup.className = 'profile__popup'
  popup.style.position = 'absolute'
  popup.style.top = `${center}px`
  popup.style.left = `${left}px`

  parent.appendChild(popup)
  attachPopupEvents()
}

function attachPopupEvents() {
  onClick('.popup__btns__cancel', handlePopupBtnCancel)
  onClick('.popup__btns__save', handlePopupBtnSave)
  $('#popup__field').focus()
}

function handlePopupBtnCancel(e) {
  onClickOff('.popup__btns__cancel', handlePopupBtnCancel)
  $('#about_tab').removeChild($('.profile__popup'))
}

function handlePopupBtnSave(e) {
  const input = $('#popup__field')
  const value = input.value.trim()
  const fieldName = input.dataset.field
  if (value) {
    setFieldValue(fieldName, value)
    onClickOff('.popup__btns__save', handlePopupBtnSave)
    $('#about_tab').removeChild($('.profile__popup'))
  }
}

function getFieldLabelAndValue(fieldName) {
  let value
  let label

  if (fieldName === 'name') {
    const firstname = $('#firstname').value
    const lastname = $('#lastname').value
    label = 'Name'
    value = `${firstname} ${lastname}`
  } else {
    label = $(`#lbl_${fieldName}`).innerHTML
    value = $(`#${fieldName}`).value
  }

  return {value, label}
}

function Popup() {

  const openPopup = (btn) => {
    const position = getPosition(btn)
    const parent = $('.profile')
    const parentPosition = getPosition(parent)

    const fieldName = btn.dataset.field

    buildPopup({
      fieldName,
      position,
      parent,
      parentPosition
    })
  }

  const getPosition = (el) => {
    const position = el.getBoundingClientRect()
    return position
  }

  return {
    openPopup,
  };
}

export default Popup()
