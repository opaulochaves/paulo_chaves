/**
 * jQuery like selector
 *
 * By default it returns the first match, but if the second parameter
 * `all` is `true` then it returns a list of all matched elements
 *
 * @param {string|Node} selector - a class, id, tag or a node (if node returns itself)
 * @param {boolean} all - whether or not return all matched elements
 */
const $ = (selector, all = false) => {
  if (typeof selector === 'object') {
    if (!all || Array.isArray(selector)) {
      return selector
    } else if (all) {
      return [selector]
    }
  }

  if (!all) {
    return document.querySelector(selector)
  }
  return document.querySelectorAll(selector)
}
export default $

/**
 * Return the parent of an element given a selector (tag, class, id)
 *
 * @param {string|Node} selector - Selector to get the parent node from
 */
export function parent(selector) {
  return $(selector).parentElement
}

/**
 * Add an onclick listener to an element given a `selector` and the `handler`
 *
 * @param {string|Node} selector - Selector to get the node or the node itself
 * @param {function} handler - Callback to be called when the event fires up
 */
export function onClick(selector, handler) {
  $(selector).addEventListener('click', handler)
}

/**
 * Remove an onclick listener from an element given a `selector` and the `handler`
 *
 * @param {string|Node} selector - Selector to get the node or the node itself
 * @param {function} handler - Callback to be called when the event fires up
 */
export function onClickOff(selector, handler) {
  $(selector).removeEventListener('click', handler)
}

/**
 * Update the attribute(s) of a node
 *
 * @param {string|Node} selector - Selector to get the parent node from
 * @param {Object} props - Object with prop and value
 *
 * Examples
 *
 *      attr('.a-class', {innerHTML: 'a text', position: 'absolute'})
 */
export function attr(selector, props) {
  const nodes = $(selector, true)
  for (const node of nodes) {
    for (const key of Object.keys(props)) {
      node[key] = props[key]
    }
  }
}

export function setFieldValue(fieldName, value, allowEmpty = true) {
  // do nothing if empty
  if (!allowEmpty && value.trim() === '') return

  if (fieldName === 'name') {
    const pos = value.indexOf(' ')
    attr(`#first${fieldName}`, {value: value.substring(0, pos)})
    attr(`#last${fieldName}`, {value: value.substring(pos+1)})
  } else {
    attr(`#${fieldName}`, {value})
  }
  attr(`.text__${fieldName}`, {innerHTML: value})
}
