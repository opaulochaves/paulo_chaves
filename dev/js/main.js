import 'babel-polyfill'
import $ from './modules/helpers'
import popup from './popup'
import tabs from './tabs'
import profile from './profile'

const btnOpenPopup = document.querySelectorAll('.profile__btn')
if (btnOpenPopup) {
  for (const btn of btnOpenPopup) {
    btn.addEventListener('click', () => {
      popup.openPopup(btn)
    })
  }
}

const btnProfileEditBtn = document.querySelector('.profile__edit__btn')
let oldValues
if (btnProfileEditBtn) {
  btnProfileEditBtn.addEventListener('click', function() {
    profile.enableEditFields()
    oldValues = {
      firstname: $('#firstname').value,
      lastname: $('#lastname').value,
      website: $('#website').value,
      phone: $('#phone').value,
      address: $('#address').value,
    }
  })
}

const btnCancel = document.querySelector('.profile__btns__cancel')
if (btnCancel) {
  btnCancel.addEventListener('click', function() {
    profile.cancelEdit(oldValues)
  })
}

const btnSave = document.querySelector('.profile__btns__save')
if (btnSave) {
  btnSave.addEventListener('click', function() {
    profile.saveForm()
  })
}

tabs.showActiveTab()
const tabItems = document.querySelectorAll('.tabs__item > a')
if (tabItems) {
  for (const item of tabItems) {
    let targetArea
    if (item.dataset && item.dataset.tab) {
      targetArea = item.dataset.tab
    }
    if (targetArea) {
      item.addEventListener('click', () => {
        tabs.openTab(item, '.tabs__area')
      })
    }
  }
}

