const gulp = require('gulp');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const gulpif = require('gulp-if');
const notify = require("gulp-notify");
const path = require('path');
const plumber = require('gulp-plumber');
const pug = require('gulp-pug');

const config = require("../config");
const mode = require("./helpers/mode");

gulp.task('html', function () {
  return gulp.src(path.join(config.root.dev, config.html.dev, './*.pug'))
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(pug({}))
    .pipe(gulp.dest(path.join(config.root.dist, config.html.dist)))
    .pipe(reload({ stream: true }));
});
