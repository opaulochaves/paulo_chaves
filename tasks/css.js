var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var glob = require('glob');
var gulpif = require('gulp-if');
var minify = require('gulp-clean-css');
var notify = require('gulp-notify');
var path = require('path');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var config = require('../config');
var mode = require('./helpers/mode');

gulp.task('css', function () {
  return gulp.src(path.join(config.root.dev, config.css.dev, 'bundle.sass'))
    .pipe(gulpif(!mode.production, sourcemaps.init()))
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(sass({
      includePaths: ['./node_modules'],
      outputStyle: 'expanded',
      sourceMap: true,
      errLogToConsole: true
    }))
    .pipe(autoprefixer({
      browsers: ['last 3 version', 'ie > 9'],
    }))
    .pipe(gulpif(mode.production, minify({
      keepSpecialComments: 0
    })))
    .pipe(gulpif(!mode.production, sourcemaps.write()))
    .pipe(gulp.dest(path.join(config.root.dist, config.css.dist)))
    .pipe(reload({ stream: true }));
});
