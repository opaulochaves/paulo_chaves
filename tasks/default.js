var gulp = require('gulp');
var runSequence = require('run-sequence');

var mode = require('./helpers/mode');

var defaultTask = function (cb) {
  mode.show();
  mode.production
    ? runSequence('clean', ['img', 'html', 'css', 'js'], 'size', cb)
    : runSequence(['img', 'html', 'css', 'js'], 'watch', cb);
};

gulp.task('default', defaultTask);
